/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.cidrdb.testing.objects;

import edu.jhmi.cidr.cidrdb.model.annotations.ManyToMany;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author btibbil1
 */
public class Watch extends ObjectBean {

    public StringProperty path = new SimpleStringProperty("");
    public StringProperty name = new SimpleStringProperty("");
    public LongProperty filecount = new SimpleLongProperty(0);
    public IntegerProperty ownerid = new SimpleIntegerProperty(-1);
    
    @ManyToMany (throughTableName = "watch_notify", referencedObjectClass = Notify.class)
    public ObservableList<Notify> notifyList = FXCollections.observableArrayList();
    
    @ManyToMany (throughTableName = "watch_whistle", referencedObjectClass = WhistleBean.class)
    public ObservableList<WhistleBean> whistleList = FXCollections.observableArrayList();
//    protected ObservableMap<WhistleType, WhistleBean> whistleMap = FXCollections.observableHashMap();

    public Watch() {
        super();
//        setupWhistles();
        setupPropertyListeners();
    }

    public Watch(String name) {
        super();
        this.name.set(name);
//        setupWhistles();
        setupPropertyListeners();
    }
    
    public boolean containsEmail(String email) {
        for (Notify notifyBean : notifyList) {
            if (notifyBean.email.equals(email)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(name.get());

        return sb.toString();
    }

    public String debugOutputString() {
        StringBuilder output = new StringBuilder();
        String ls = System.getProperty("line.separator");

        output.append(ls)
                .append("name: ").append(name.get())
                .append(ls)
                .append("PK: ").append(id)
                .append(ls)
                .append("path: ").append(path.get())
                .append(ls)
                .append("filecount: ").append(filecount.get())
                .append(ls)
                .append("owner id: ").append(ownerid)
                .append(ls);

        for (Notify notifyBean : notifyList) {
            output.append("\t")
                    .append(notifyBean.debugOutputString())
                    .append(ls);
        }

//        System.out.println(getWhistleMap().size());

        for (WhistleBean whistleBean : whistleList) {
            output.append("\t")
                    .append(whistleBean.debugOutputString())
                    .append(ls);
        }

        return output.toString();
    }

//    public WatchBean(String name, String path, long filecount, int ownerid, List<NotifyBean> notifyList, List<WhistleBean> whistleList) {
//        super(id);
//        this.name.set(name);
//        this.path.set(path);
//        this.filecount.set(filecount);
//        this.ownerID.set(ownerid);
//        this.notifyList.addAll(notifyList);
//        addWhistles(whistleList);
//        setupWhistles();
//        setupPropertyListeners();
//    }

//    private void setupWhistles() {
//        for (WhistleType whistleType : WhistleType.values()) {
//            if (!whistleMap.containsKey(whistleType)) {
//                whistleMap.put(whistleType, new Whistle(whistleType, whistleType.getDefaulValue()));
//            }
//
//            whistleMap.get(whistleType).getSavedProperty().addListener(new ChangeListener<Boolean>() {
//                @Override
//                public void changed(ObservableValue<? extends Boolean> observableValue, Boolean oldValue, Boolean newValue) {
//                    getSavedProperty().set(newValue);
//                }
//            });
//        }
//    }
    

//    public Whistle getWhistle(WhistleType whistleType) {
//        return getWhistleMap().get(whistleType);
//    }
//
//    public Whistle getWhistleByID(Integer whistleID) {
//        return getWhistleMap().get(WhistleType.getWhistleType(whistleID));
//    }

//    public ObservableList<Notify> getNotifyList() {
//        return notifyList;
//    }

//    public Collection<Whistle> getWhistleList() {
//        return Collections.unmodifiableCollection(getWhistleMap().values());
//    }

//    public void addWhistle(Whistle whistleBean) {
//        getWhistleMap().put(whistleBean.getWhistleType(), whistleBean);
//    }

//    private void addWhistles(List<WhistleBean> whistleBeanList) {
//        for (WhistleBean whistleBean : whistleBeanList) {
//            addWhistle(whistleBean);
//        }
//    }

//    /**
//     * @return the path
//     */
//    public String getPath() {
//        return path.get();
//    }
//
//    public StringProperty getPathProperty() {
//        return path;
//    }
//
//    /**
//     * @return the name
//     */
//    public String getName() {
//        return name.get();
//    }
//
//    public StringProperty getNameProperty() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name.set(name);
//    }
//
//    /**
//     * @return the filecount
//     */
//    public long getFilecount() {
//        return filecount.get();
//    }
//    
//    public LongProperty getFilecountProperty() {
//        return filecount;
//    }
//
//    /**
//     * @return the ownerID
//     */
//    public int getOwnerID() {
//        return ownerID.get();
//    }
//    
//    public IntegerProperty getOwnerIDProperty() {
//        return ownerID;
//    }
//
//    /**
//     * @return the whistleMap
//     */
//    public ObservableMap<WhistleType, Whistle> getWhistleMap() {
//        return whistleMap;
//    }
}
