/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.cidrdb.testing.objects;

import edu.jhmi.cidr.cidrdb.model.annotations.ColumnInfo;
import edu.jhmi.cidr.cidrdb.model.annotations.TableInfo;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author btibbil1
 */
@TableInfo(tableName = "whistle")
public class WhistleBean extends ObjectBean {

    public IntegerProperty whistle_type = new SimpleIntegerProperty();
    public StringProperty whistle_attribute = new SimpleStringProperty("");
    
    @ColumnInfo(columnName = "enabled")
    public BooleanProperty enabledProperty = new SimpleBooleanProperty(false);

    public WhistleBean() {
        setupPropertyListeners();
    }

    public BooleanProperty getEnabledProperty() {
        return enabledProperty;
    }

    public String debugOutputString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ID: ").append(whistle_type.get()).append(", ")
                .append("ATTRIBUTE: ").append(whistle_attribute.get()).append(", ")
                .append("Enabled: ").append(enabledProperty.get());

        return sb.toString();
    }
//    public Whistle(WhistleType whistleType, String whistleAttribute) {
//        super();
////        this.whistleType = whistleType;
//        this.whistleAttribute.set(whistleAttribute);
//        setupPropertyListeners();
//    }
    /**
     * @return the WhistleType
     */
//    public WhistleType getWhistleType() {
//        return whistleType;
//    }
//    public boolean isEnabled() {
//        return enabledProperty.get();
//    }
//    
//    /**
//     * @return the whistleAttribute
//     */
//    public String getWhistleAttribute() {
//        return whistle_attribute.get();
//    }
//
//    public StringProperty getWhistleAttributeProperty() {
//        return whistle_attribute;
//    }
//    
//    /**
//     * @param whistleAttribute the whistleAttribute to set
//     */
//    public void setWhistleAttribute(String whistleAttribute) {
//        this.whistle_attribute.set(whistleAttribute);
//    }
//    @Override
//    public String toString() {
//        return String.valueOf(whistleType);
//    }
//    public enum WhistleType {
//        DIRECT_SUBDIRECTORY(1, ""), PERCENTAGE(2, "50");
//        
//        private final int id;
//        private final String defaulValue;
//        private final static Map<Integer, WhistleType> whistleTypeMap = new HashMap<>();
//
//        static {
//            for (WhistleType whistleType : WhistleType.values()) {
//                whistleTypeMap.put(whistleType.getId(), whistleType);
//            }
//        }
//
//        private WhistleType(int id, String defaulValue) {
//            this.id = id;
//            this.defaulValue = defaulValue;
//        }
//
//        public int getId() {
//            return id;
//        }
//
//        public String getDefaulValue() {
//            return defaulValue;
//        }
//        
//        public static WhistleType getWhistleType(int id) {
//            return whistleTypeMap.get(id);
//        }
//    }
}
