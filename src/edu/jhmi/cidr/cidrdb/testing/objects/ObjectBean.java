/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.cidrdb.testing.objects;

import edu.jhmi.cidr.cidrdb.model.data.DBObject;
import java.lang.reflect.Field;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;

/**
 *
 * @author btibbil1
 */
public class ObjectBean extends DBObject {
    private BooleanProperty saved = new SimpleBooleanProperty(true);

    public BooleanProperty getSavedProperty() {
        return saved;
    }
    
    public boolean isSaved(){
        return saved.get();
    }
    
    public void resetSavedFlag(){
        saved.set(true);
    }
    
    protected void setupPropertyListeners() {
        for (Field field : this.getClass().getDeclaredFields()) {
            if(field.getName().equals("saved")){
                continue;
            }
            
            try {
                Object fieldObject = field.get(this);
                if (fieldObject instanceof Property) {
                    Property prop = (Property) fieldObject;
                    prop.addListener(new ChangeListener() {
                        @Override
                        public void changed(ObservableValue ov, Object t, Object t1) {
                            saved.set(false);
                        }
                    });
                } else if (fieldObject instanceof ObservableList) {
                    ObservableList observableList = (ObservableList) fieldObject;
                    observableList.addListener(new ListChangeListener() {
                        @Override
                        public void onChanged(ListChangeListener.Change change) {
                            saved.set(false);
                        }
                    });
                } else if (fieldObject instanceof ObservableMap) {
                    ObservableMap observableMap = (ObservableMap) fieldObject;
                    observableMap.addListener(new MapChangeListener() {
                        @Override
                        public void onChanged(MapChangeListener.Change change) {
                            saved.set(false);
                        }
                    });
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
