/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.cidrdb.testing;

import com.google.common.base.Joiner;
import edu.jhmi.cidr.cidrdb.model.annotations.ColumnInfo;
import edu.jhmi.cidr.cidrdb.model.annotations.ForeignKey;
import edu.jhmi.cidr.cidrdb.model.annotations.ManyToMany;
import edu.jhmi.cidr.cidrdb.model.annotations.OneToMany;
import edu.jhmi.cidr.cidrdb.model.data.DBObject;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author btibbil1
 */
public class First_Table extends DBObject {
//    @ManyToMany (throughTableName = "line_items", referencedObjectClass = Second_Table.class)
//    public List<Second_Table> objects = new ArrayList<>();
    public String text_value;
    public Type type;
    
    @ForeignKey(referencedObjectClass = ForeignTest.class, columnName = "foreign_test_id")
    public ForeignTest foreignTest;
    
    @OneToMany(referencedObjectClass = Second_Table.class, parentForeignKeyColumnName = "first_table_id")
    public List<Second_Table> secondTables;
    
    public Double decimal_test = null;

    public First_Table() {
        super();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        
        sb.append(text_value).append(" : ").append(type).append(" : ").append(foreignTest.name).append(" : ").append(decimal_test)
                .append("\n").append(Joiner.on("\n").join(secondTables));
        
        return sb.toString();
    }
    
    
     
    public enum Type {
        TEST1, TEST2;
    }
}
