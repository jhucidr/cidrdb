/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.cidrdb.model.db.connections;

import edu.jhmi.cidr.cidrdb.model.db.exceptions.DBException;
import java.util.List;
import java.util.Map;

/**
 *
 * @author asanch15
 */
public interface Database {
    public void connect(String user, String pass, String dbAddr,
            int port, String dbName) throws DBException;
    public void disconnect() throws DBException;
    public boolean isConnected();
    
    public void startTransaction() throws Exception;
    public void commitTransaction() throws Exception;
    public void rollbackTransaction() throws Exception;
    
    public Map<String, Object> getObjectData(String tableName, int pk) 
            throws DBException;
    public List<Map<String, Object>> getObjects(String tableName)
            throws DBException;
    public List<Map<String, Object>> getObjects(String tableName, String whereClause)
            throws DBException;
    
    public void updateObject(String tableName, int pk, 
            Map<String, Object> objData) throws DBException;
    
    public void deleteObject(String tableName, int pk) throws DBException;
    public void deleteObject(String tableName, String whereClause) throws DBException;
    
    public int insertObject(String tableName, Map<String, Object> objData) throws
            DBException;
    
    public String getPrimaryKeyColumnName();
}
