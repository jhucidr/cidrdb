/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.cidrdb.model.db.connections;

import com.google.common.base.Joiner;
import edu.jhmi.cidr.cidrdb.Constants.DBConstants;
import edu.jhmi.cidr.cidrdb.model.db.exceptions.DBException;
import edu.jhmi.cidr.cidrdb.model.db.exceptions.DBObjectNotFound;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 *
 * @author asanch15
 */
public class MySQLDatabase implements Database {

    private static final int TIMEOUT = 1000;
    private static final boolean PRINT_STACK_TRACES = true;

    private final String primaryKeyColumnName;
    private boolean printSQLStatements = false;
    private Connection conn;

    public MySQLDatabase() {
        this.primaryKeyColumnName = DBConstants.DEFAULT_ID_COLUMN_NAME;
        Runtime.getRuntime().addShutdownHook(new ShutDownHook(this));
    }

    public MySQLDatabase(String primaryKeyColumnName) {
        this.primaryKeyColumnName = primaryKeyColumnName;
        Runtime.getRuntime().addShutdownHook(new ShutDownHook(this));
    }

    public void setPrintSQLStatements(boolean printSQLStatements) {
        this.printSQLStatements = printSQLStatements;
    }

    private Map<String, Object> validateData(Map<String, Object> data) {
        if (data.containsKey(primaryKeyColumnName)) {
            data.remove(primaryKeyColumnName);
        }

        return data;
    }

    private String createSQLVariableListString(int numVars) {
        List<String> vars = new ArrayList<>();

        for (int i = 0; i < numVars; i++) {
            vars.add("?");
        }

        return Joiner.on(",").join(vars);
    }

    public String getPrimaryKeyColumnName() {
        return primaryKeyColumnName;
    }

    private PreparedStatement createSelectStatement(String tablename) throws SQLException {
        PreparedStatement stmt = conn.prepareStatement("SELECT * FROM " + tablename);

        return stmt;
    }

    private PreparedStatement createSelectStatement(String tablename, int id) throws SQLException {
        PreparedStatement stmt = conn.prepareStatement("SELECT * FROM " + tablename + " WHERE " + primaryKeyColumnName + " = ?");

        stmt.setInt(1, id);

        return stmt;
    }

    private PreparedStatement createSelectStatement(String tablename, String whereClause) throws SQLException {
        PreparedStatement stmt = conn.prepareStatement("SELECT * FROM " + tablename + " WHERE " + whereClause);

        return stmt;
    }

    private PreparedStatement createInsertStatement(String tablename, Map<String, Object> data) throws SQLException {
        Set<String> keySet = data.keySet();
        String colString = Joiner.on(",").join(keySet);
        StringBuilder sb = new StringBuilder();
        int count = 1;
        PreparedStatement stmt;
//        = conn.prepareStatement("INSERT INTO " + tablename + " () VALUES (?)", Statement.RETURN_GENERATED_KEYS);

        sb.append("INSERT INTO ");
        sb.append(tablename);
        sb.append("(").append(colString).append(") ");
        sb.append("VALUES (").append(createSQLVariableListString(keySet.size())).append(")");

        stmt = conn.prepareStatement(sb.toString(), Statement.RETURN_GENERATED_KEYS);

        for (String key : keySet) {
            stmt.setObject(count, data.get(key));
            count++;
        }

        return stmt;
    }

    private PreparedStatement createUpdateStatment(String tablename, int id, Map<String, Object> data) throws SQLException {
        StringBuilder sb = new StringBuilder();
        String updateStr = Joiner.on(",").join(mapKeysToVariableString(data));
        int varCount = 1;

//        String values = mapToSQLValues(data, true, true);
        PreparedStatement stmt;

        sb.append("UPDATE ").append(tablename);
        sb.append(" SET ");
        sb.append(updateStr);
        sb.append(" WHERE ").append(primaryKeyColumnName).append(" = ?");

        stmt = conn.prepareStatement(sb.toString());

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            Object value = entry.getValue();

            stmt.setObject(varCount, value);
            varCount++;
        }

        stmt.setInt(varCount, id);

        return stmt;
    }

    private PreparedStatement createDeleteStatement(String tablename, String whereClause) throws SQLException {
        StringBuilder sb = new StringBuilder();
        PreparedStatement stmt;

        sb.append("DELETE FROM ").append(tablename).append(" WHERE ").append(whereClause);
        stmt = conn.prepareStatement(sb.toString());

        return stmt;
    }

    private List<String> mapKeysToVariableString(Map<String, Object> map) {
        List<String> keyStrings = new ArrayList<>();

        for (String key : map.keySet()) {
            StringBuilder sb = new StringBuilder();

            sb.append(key).append(" = ?");
            keyStrings.add(sb.toString());
        }

        return keyStrings;
    }

//    private static String mapToSQLValues(Map<String, Object> data, boolean mapKeys,
//            boolean mapValues) {
//        List<String> values = new ArrayList<>(data.size());
//
//        for (Map.Entry<String, Object> en : data.entrySet()) {
//            String value;
//            if (en.getKey().equals(DBConstants.ID_COLUMN_NAME)) {
//                continue;
//            }
//
//            if (mapKeys && mapValues) {
//                value = (en.getKey() + "='" + en.getValue() + "'");
//            } else if (mapKeys) {
//                value = en.getKey();
//            } else if (mapValues) {
//                value = "'" + en.getValue() + "'";
//            } else {
//                value = "";
//            }
//
//            values.add(value);
//        }
//
//        return Joiner.on(",").join(values);
//    }
    private static void throwDBException(Exception e)
            throws DBException {
        if (PRINT_STACK_TRACES) {
            e.printStackTrace();
        }
        throw new DBException(e.getMessage());
    }

    @Override
    public void connect(String user, String pass, String dbAddr, int port, String dbName) throws DBException {
        Properties props = new Properties();

        props.put("user", user);
        props.put("password", pass);

        try {
            conn = DriverManager.getConnection(
                    "jdbc:" + "mysql://" + dbAddr + ":" + port + "/" + dbName, props);
            conn.setAutoCommit(false);
        } catch (SQLException e) {
            throwDBException(e);
        }
    }

    @Override
    public void disconnect() throws DBException {
        try {
            conn.close();
            conn = null;
        } catch (SQLException e) {
            throwDBException(e);
        }
    }

    @Override
    public boolean isConnected() {
        if (conn != null) {
            try {
                if (conn.isValid(TIMEOUT)) {
                    return true;
                }
            } catch (SQLException e) {
                if (PRINT_STACK_TRACES) {
                    e.printStackTrace();
                }
                return false;
            }
        }

        return false;
    }

    @Override
    public Map<String, Object> getObjectData(String tableName, int id) throws DBException {
        Map<String, Object> resultMap = new HashMap<>();
        PreparedStatement statement;
        ResultSet rs;
        ResultSetMetaData rsmd;

        if (conn == null) {
            throw new DBException("Not connected to a database");
        }

        try {
            statement = createSelectStatement(tableName, id);

            if (printSQLStatements) {
                System.out.println(statement);
            }

            rs = statement.executeQuery();

            if (rs.last() == true) {
                rsmd = rs.getMetaData();

                for (int i = 1; i < rsmd.getColumnCount() + 1; i++) {
                    resultMap.put(rsmd.getColumnName(i), rs.getObject(i));
                }
            } else {
                throw new DBObjectNotFound(tableName, id);
            }

            statement.close();
        } catch (SQLException e) {
            throwDBException(e);
        }

        return resultMap;
    }

    @Override
    public List<Map<String, Object>> getObjects(String tableName) throws DBException {
        return getObjects(tableName, null);
    }

    @Override
    public List<Map<String, Object>> getObjects(String tableName, String whereClause) throws DBException {
        List<Map<String, Object>> dbObjects = new ArrayList<>();
        PreparedStatement statement;
//        String query = "SELECT * FROM " + tableName;
        ResultSet rs;
        ResultSetMetaData rsmd;

        if (conn == null) {
            throw new DBException("Not connected to a database");
        }

        try {
            if (whereClause != null && !whereClause.isEmpty()) {
                statement = createSelectStatement(tableName, whereClause);
            } else {
                statement = createSelectStatement(tableName);
            }

            if (printSQLStatements) {
                System.out.println(statement);
            }

            rs = statement.executeQuery();

            while (rs.next()) {
                Map<String, Object> objData = new HashMap<>();
                rsmd = rs.getMetaData();

                for (int i = 1; i < rsmd.getColumnCount() + 1; i++) {
                    objData.put(rsmd.getColumnName(i), rs.getObject(i));
                }

                dbObjects.add(objData);
            }

            statement.close();
        } catch (SQLException e) {
            throwDBException(e);
        }

        return dbObjects;
    }

    @Override
    public void deleteObject(String tableName, int id) throws DBException {
        deleteObject(tableName, primaryKeyColumnName + " = " + id);
    }

    @Override
    public void deleteObject(String tableName, String whereClause) throws DBException {
        PreparedStatement statement;

        if (conn == null) {
            throw new DBException("Not connected to database");
        }

        try {
            statement = createDeleteStatement(tableName, whereClause);

            if (printSQLStatements) {
                System.out.println(statement);
            }

            if (statement.executeUpdate() < 1) {
                throw new DBObjectNotFound(tableName, whereClause);
            }
        } catch (SQLException e) {
            throwDBException(e);
        }
    }

    @Override
    public void updateObject(String tableName, int id,
            Map<String, Object> objData) throws DBException {
        PreparedStatement statement;
        Map<String, Object> validatedData = validateData(objData);

        if (conn == null) {
            throw new DBException("Not connected to a database");
        }

        try {
            statement = createUpdateStatment(tableName, id, validatedData);

            if (printSQLStatements) {
                System.out.println(statement);
            }

            if (statement.executeUpdate() < 1) {
                throw new DBObjectNotFound(tableName, id);
            }

            statement.close();
        } catch (SQLException e) {
            throwDBException(e);
        }
    }

    @Override
    public int insertObject(String tableName, Map<String, Object> objData) throws DBException {
        PreparedStatement statement;
        Map<String, Object> validatedData = validateData(objData);
//        String objKeys = mapToSQLValues(objData, true, false);
//        String objValues = mapToSQLValues(objData, false, true);
//        String query = "INSERT INTO " + tableName + "(" + objKeys + ")" + " VALUES (" + objValues + ")";
        ResultSet rs;
        int id = DBConstants.NO_ID;

        if (conn == null) {
            throw new DBException("Not connected to a database");
        }

        try {
            statement = createInsertStatement(tableName, validatedData);

            if (printSQLStatements) {
                System.out.println(statement);
            }

            statement.executeUpdate();
            rs = statement.getGeneratedKeys();

            if (rs.next()) {
                id = rs.getInt(1);
            } else {
                id = DBConstants.NO_ID;
            }

            statement.close();
        } catch (SQLException e) {
            throwDBException(e);
        }

        return id;
    }

    @Override
    public void startTransaction() throws Exception {
        if (conn == null) {
            throw new DBException("Not connected to a database");
        }
//        conn.setAutoCommit(false);
    }

    @Override
    public void commitTransaction() throws Exception {
        if (conn == null) {
            throw new DBException("Not connected to a database");
        }
        conn.commit();
//        conn.setAutoCommit(true);
    }

    @Override
    public void rollbackTransaction() throws Exception {
        if (conn == null) {
            throw new DBException("Not connected to a database");
        }
        conn.rollback();
//        conn.setAutoCommit(true);
    }

    private class ShutDownHook extends Thread {

        private final Database db;

        public ShutDownHook(Database db) {
            super();
            this.db = db;
        }

        @Override
        public void run() {
            try {
                if (db != null && db.isConnected()) {
                    db.disconnect();
                }
            } catch (DBException e) {
                e.printStackTrace();
            }
        }
    }

    private enum StatmentType {

        INSERT, UPDATE, REMOVE, SELECT;
    }
}
