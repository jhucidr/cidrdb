/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.cidrdb.model.db.exceptions;

/**
 *
 * @author asanch15
 */
public class DBObjectNotFound extends DBException {
    public DBObjectNotFound(){
        super();
    }
    
    public DBObjectNotFound(String msg){
        super(msg);
    }
    
    /**
     * Formats tableName and pk into an error message
     * 
     * @param tableName
     * @param pk 
     */
    public DBObjectNotFound(String tableName, int pk){
        super("Not object found at " + tableName + " with primary key " + pk);
    }
    
       public DBObjectNotFound(String tableName, String whereClause){
        super("Not object found at " + tableName + " WHERE " + whereClause);
    }
}
