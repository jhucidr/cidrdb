/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.cidrdb.model.data;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import edu.jhmi.cidr.cidrdb.Constants.DBConstants;
import edu.jhmi.cidr.cidrdb.model.annotations.ColumnInfo;
import edu.jhmi.cidr.cidrdb.model.annotations.Exclude;
import edu.jhmi.cidr.cidrdb.model.annotations.ForeignKey;
import edu.jhmi.cidr.cidrdb.model.annotations.ManyToMany;
import edu.jhmi.cidr.cidrdb.model.annotations.OneToMany;
import edu.jhmi.cidr.cidrdb.model.annotations.TableInfo;
import edu.jhmi.cidr.cidrdb.model.db.connections.Database;
import edu.jhmi.cidr.cidrdb.model.db.exceptions.DBException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.beans.property.Property;

/**
 *
 * @author asanch15
 */
public class CidrDB {

    private final Database db;
    private final Table<Class, Integer, DBObject> cacheTable = HashBasedTable.create();

    private boolean autoCommit = true;
    private boolean transactionInProcess = false;

    public CidrDB(Database db) {
        this.db = db;

    }

    public CidrDB(Database db, boolean autoCommit) {
        this.db = db;
        this.autoCommit = autoCommit;
    }

    public boolean isAutoCommit() {
        return autoCommit;
    }

    public void setAutoCommit(boolean autoCommit) {
        this.autoCommit = autoCommit;
    }

    private String getTableName(Class dboClass) {
        String tableName;

        if (dboClass.isAnnotationPresent(TableInfo.class)) {
            TableInfo tableInfo = (TableInfo) dboClass.getAnnotation(TableInfo.class);

            tableName = tableInfo.tableName();
        } else {
            tableName = dboClass.getSimpleName().toLowerCase();
        }

        return tableName;
    }

    private <T extends DBObject> T getDBObjectFromCache(Class dboClass, int id) {
        return (T) cacheTable.get(dboClass, id);
    }

    private void addDBObjectToCache(DBObject dbo) {
        cacheTable.put(dbo.getClass(), dbo.getID(), dbo);
    }

    private String getColumnName(Field field) {
        String columnName = field.getName().toLowerCase();

        if (field.isAnnotationPresent(ColumnInfo.class)) {
            ColumnInfo columnInfo = (ColumnInfo) field.getAnnotation(ColumnInfo.class);

            columnName = columnInfo.columnName();
        } else if (field.isAnnotationPresent(ForeignKey.class)) {
            ForeignKey foreignKey = (ForeignKey) field.getAnnotation(ForeignKey.class);
            String foreignKeyColumnName = foreignKey.columnName().trim();

            if (foreignKeyColumnName.isEmpty()) {
                columnName = field.getName().toLowerCase() + "_" + db.getPrimaryKeyColumnName();
            } else {
                columnName = foreignKeyColumnName;
            }
        }

        return columnName;
    }

    private String getThroughTableColumnName(Class dboClass, String columnName) {
        if (columnName.isEmpty()) {
            return getTableName(dboClass) + "_id";
        }

        return columnName;
    }

    private Map<String, Object> createLineItem(Class parentObjectClass, ManyToMany manyToMany, int parentID, int referenceID) {
        Map<String, Object> lineItem = new HashMap<>();
        String parentColumnName = getThroughTableColumnName(parentObjectClass, manyToMany.parentObjectColumnName());
        String referencedColumnName = getThroughTableColumnName(manyToMany.referencedObjectClass(), manyToMany.referencedObjectColumnName());

        lineItem.put(parentColumnName, parentID);
        lineItem.put(referencedColumnName, referenceID);

        return lineItem;
    }

    private String createLineItemWhereClause(Class parentObjectClass, ManyToMany manyToMany, int parentID, int referenceID) {
        StringBuilder sb = new StringBuilder();
        String parentColumnName = getThroughTableColumnName(parentObjectClass, manyToMany.parentObjectColumnName());
        String referencedColumnName = getThroughTableColumnName(manyToMany.referencedObjectClass(), manyToMany.referencedObjectColumnName());

        sb.append(parentColumnName).append(" = ").append(parentID)
                .append(" AND ")
                .append(referencedColumnName).append(" = ").append(referenceID);

        return sb.toString();
    }

    public <T extends DBObject> List<T> getAll(Class dboClass) throws Exception {
        return getWhere(dboClass, null, true);
    }

    public <T extends DBObject> List<T> getWhere(Class dboClass, String whereClause) throws Exception {
        return getWhere(dboClass, whereClause, true);
    }

    private <T extends DBObject> List<T> getWhere(Class dboClass, String whereClause, boolean clearCache) throws Exception {
        String tableName = getTableName(dboClass);
        List<Map<String, Object>> objDataMaps;
        List<T> objList = new ArrayList<>();

        if (whereClause != null) {
            objDataMaps = db.getObjects(tableName, whereClause);
        } else {
            objDataMaps = db.getObjects(tableName);
        }

        for (Map<String, Object> objData : objDataMaps) {
            T dbObject = createDBObject(objData, dboClass, false);
            objList.add(dbObject);
        }

        if (clearCache) {
            cacheTable.clear();
        }

        return objList;
    }

    private Map<Integer, DBObject> getDBObjectsByID(Collection<DBObject> objects) {
        Map<Integer, DBObject> dbObjectMap = new HashMap<>();

        for (DBObject dbObject : objects) {
            dbObjectMap.put(dbObject.getID(), dbObject);
        }

        return dbObjectMap;
    }

    private void postSave(DBObject dbo, boolean updateReferences) throws Exception {
        for (Field field : dbo.getClass().getFields()) {
            if (field.isAnnotationPresent(ManyToMany.class)) {
                ManyToMany manyToMany = field.getAnnotation(ManyToMany.class);
                List<DBObject> dbReferencedObjects = getReferencedObjects(manyToMany.throughTableName(),
                        dbo.getClass(), manyToMany,
                        dbo.getID(), false);
                Map<Integer, DBObject> referencedDBObjectMap = getDBObjectsByID(dbReferencedObjects);
                Collection<DBObject> currentReferencedObjects;

                if (!manyToMany.hashMapKey().isEmpty()) {
                    currentReferencedObjects = ((Map) field.get(dbo)).values();
                } else {
                    currentReferencedObjects = (Collection) field.get(dbo);
                }

                for (DBObject referencedObject : currentReferencedObjects) {
                    if (updateReferences) {
                        save(referencedObject, true);
                    }

                    if (!referencedDBObjectMap.keySet().contains(referencedObject.getID())) {
                        db.insertObject(manyToMany.throughTableName(),
                                createLineItem(dbo.getClass(),
                                        manyToMany,
                                        dbo.getID(),
                                        referencedObject.getID()));
                    } else {
//                        dbReferencedObjects.remove(referencedObject);
                        referencedDBObjectMap.remove(referencedObject.getID());
                    }
                }

                //Remove any left over objects
                if (updateReferences) {
                    for (DBObject referencedObject : referencedDBObjectMap.values()) {
                        removeManyToManyRelationship(dbo, referencedObject, manyToMany);
                    }
                }
            }
//            else if(field.isAnnotationPresent(OneToMany.class)){
//                OneToMany oneToMany = field.getAnnotation(OneToMany.class);
//                
//            }
        }
    }

    public void startTransaction() throws Exception {
        db.startTransaction();
        transactionInProcess = true;
    }

    public void commitTransaction() throws Exception {
        db.commitTransaction();
        transactionInProcess = false;
    }

    public void rollbackTransaction() throws Exception {
        db.rollbackTransaction();
        transactionInProcess = false;
    }

    /**
     *
     * @param dbo
     * @param updateReferences update objects this object references
     * @throws Exception
     */
    public void save(DBObject dbo, Boolean updateReferences) throws Exception {
        if (autoCommit == false && transactionInProcess == false) {
            throw new Exception("Must start a transaction before saving when auto commit mode is off");
        }

        try {
            if (autoCommit) {
                db.startTransaction();
            }

            if (dbo.getID() == DBConstants.NO_ID) {
                Integer id = insertObject(dbo);
                dbo.setID(id);
            } else {
                update(dbo);
            }

            postSave(dbo, updateReferences);

            if (autoCommit) {
                db.commitTransaction();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            if (autoCommit) {
                db.rollbackTransaction();
            }
            throw ex;
        }
    }

    public Integer insertObject(DBObject obj) throws Exception {
        String tableName = getTableName(obj.getClass());
        int id = db.insertObject(tableName, fieldsToMapData(obj));

        return id;
    }

    /**
     * Update specific fields on the database table
     *
     * @param obj
     * @param fields
     * @throws Exception
     */
    public void update(DBObject obj, String... fields) throws Exception {
        String tableName = getTableName(obj.getClass());

        db.updateObject(tableName, obj.getID(), fieldsToMapData(obj, fields));
    }

    public void removeObject(DBObject dbo) throws Exception {
        String tableName = getTableName(dbo.getClass());

        db.deleteObject(tableName, dbo.getID());

        //Clean up any many to many relationships this object had
        for (Field field : dbo.getClass().getFields()) {
            if (field.isAnnotationPresent(ManyToMany.class
            )) {
                ManyToMany manyToMany = field.getAnnotation(ManyToMany.class);
                Collection<DBObject> currentReferencedObjects;

                if (manyToMany.hashMapKey()
                        .isEmpty()) {
                    currentReferencedObjects = (Collection) field.get(dbo);
                } else {
                    currentReferencedObjects = ((Map) field.get(dbo)).values();
                }

                for (DBObject referencedObject : currentReferencedObjects) {
                    removeManyToManyRelationship(dbo, referencedObject, manyToMany);
                }
            }
        }

    }

    private void removeManyToManyRelationship(DBObject parentObject, DBObject referencedObject, ManyToMany manyToMany) throws Exception {
        String whereClause = createLineItemWhereClause(parentObject.getClass(), manyToMany, parentObject.getID(), referencedObject.getID());

        db.deleteObject(manyToMany.throughTableName(), whereClause);
    }

    private Map<String, Object> fieldsToMapData(DBObject dbo, String... fields) throws Exception {
        Map<String, Object> fieldMap = new HashMap<>();
        List<String> allowedFields = null;

        if (fields.length > 0) {
            allowedFields = Arrays.asList(fields);
        }

        for (Field field : dbo.getClass().getFields()) {

            if (allowedFields != null && !allowedFields.contains(field.getName())) {
                continue;
            }

            if (!field.isAnnotationPresent(Exclude.class) && !Modifier.isTransient(field.getModifiers())) {
                String columnName = getColumnName(field);

                if (field.isAnnotationPresent(ForeignKey.class)) {
                    DBObject referencedObject = (DBObject) field.get(dbo);
                    Integer foreignKey = referencedObject.getID();

                    if (foreignKey == DBConstants.NO_ID) {
                        foreignKey = insertObject(referencedObject);
                    } else {
                        update(referencedObject);
                    }

                    fieldMap.put(columnName, foreignKey);
                } else if (field.isAnnotationPresent(ManyToMany.class) || field.isAnnotationPresent(OneToMany.class)) {
                    //Skip
                } else if (field.isAnnotationPresent(OneToMany.class)) {
                    //Skip
                } else {
                    if (Property.class.isAssignableFrom(field.getType())) {
                        Property property = (Property) field.get(dbo);

                        fieldMap.put(columnName, property.getValue());
                    } else if (Enum.class.isAssignableFrom(field.getType())) {
                        Enum enumField = (Enum) field.get(dbo);

                        fieldMap.put(columnName, enumField.name());
                    } else {
                        fieldMap.put(columnName, field.get(dbo));
                    }
                }
            }
        }

        return fieldMap;
    }

    private <T extends DBObject> T createDBObject(Map<String, Object> objData,
            Class dboClass, boolean skipManyToManyFields) throws Exception {
        T dbObject;
        Integer dbObjectID = DBConstants.NO_ID;
        List<ForeignKeyRequest> foreignKeyFields = new ArrayList<>();
        List<Field> manyToManyFields = new ArrayList<>();
        List<Field> oneToManyFields = new ArrayList<>();

        if (objData.containsKey(db.getPrimaryKeyColumnName())) {
            dbObjectID = (Integer) objData.get(db.getPrimaryKeyColumnName());
        } else {
            throw new Exception("DB tuple has no id !");
        }

        dbObject = getDBObjectFromCache(dboClass, dbObjectID);

        //If the cache did not contain the DBObject then lets create one ourselves
        if (dbObject == null) {
            dbObject = (T) dboClass.getDeclaredConstructor().newInstance();

            for (Field field : dboClass.getFields()) {
                if (!field.isAnnotationPresent(Exclude.class) && !Modifier.isTransient(field.getModifiers())) {
                    String columnName = getColumnName(field);
                    Object columnData = objData.get(columnName);

                    if (columnData != null) {
                        if (field.isAnnotationPresent(ForeignKey.class)) {
                            foreignKeyFields.add(new ForeignKeyRequest(field, (Integer) columnData));
//                            Integer foreignKey = (Integer) columnData;
//                            ForeignKey foreignKeyAnnotation = field.getAnnotation(ForeignKey.class);
//                            DBObject referencedObject = getObject(foreignKeyAnnotation.referencedObjectClass(), foreignKey);
//
//                            field.set(dbObject, referencedObject);
                        } else {
                            //Support for JavaFX properties
                            if (Property.class.isAssignableFrom(field.getType())) {
                                Property property = (Property) field.get(dbObject);

                                property.setValue(columnData);
                            } else if (Enum.class.isAssignableFrom(field.getType())) {
                                String enumName = columnData.toString();

                                field.set(dbObject, Enum.valueOf((Class<Enum>) field.getType(), enumName));
                            } else {
                                if (BigDecimal.class.isAssignableFrom(columnData.getClass())) {
                                    field.set(dbObject, ((BigDecimal) columnData).doubleValue());
                                } else {
                                    field.set(dbObject, columnData);
                                }
                            }
                        }
                    } else if (skipManyToManyFields == false) {
                        if (field.isAnnotationPresent(ManyToMany.class)) {
                            manyToManyFields.add(field);
                        } else if (field.isAnnotationPresent(OneToMany.class)) {
                            oneToManyFields.add(field);
                        }
                    }
                }
            }
            addDBObjectToCache(dbObject);
        }

        //Handle any many to many fields after adding the object to our cache 
        //So in the case that another object has a reference to this one it will grab the cache version rather then performing a query
        for (Field field : manyToManyFields) {
            handleManyToManyField(field, dbObject);
        }
        for (Field field : oneToManyFields) {
            handleOneToManyField(field, dbObject);
        }
        for (ForeignKeyRequest foreignKeyRequest : foreignKeyFields) {
            handleForeignKeyField(foreignKeyRequest, dbObject);
        }

        return dbObject;

    }

    private <T extends DBObject> void handleOneToManyField(Field oneToManyField, T parentObject) throws Exception {
        OneToMany oneToMany = oneToManyField.getAnnotation(OneToMany.class);
        Class referencedClass = oneToMany.referencedObjectClass();
        String parentFKColumnName = oneToMany.parentForeignKeyColumnName();
        List<T> referencedObjects = getWhere(referencedClass, parentFKColumnName + " = " + parentObject.getID(), false);
        List fieldList = (List) oneToManyField.get(parentObject);

        if (fieldList == null) {
            fieldList = ArrayList.class.newInstance();
            oneToManyField.set(parentObject, fieldList);
        } else {
            fieldList.clear();
        }

        fieldList.addAll(referencedObjects);
    }

    private void handleManyToManyField(Field manyToManyField, DBObject parentObject) throws Exception {
        ManyToMany manyToMany = manyToManyField.getAnnotation(ManyToMany.class);
        List<DBObject> dbObjects = getReferencedObjects(manyToMany.throughTableName(), parentObject.getClass(), manyToMany, parentObject.getID(), true);

        if (!manyToMany.hashMapKey().isEmpty()) {
            Map referencedObjectMap = (Map) manyToManyField.get(parentObject);
            Field hashMapKeyField = manyToMany.referencedObjectClass().getField(manyToMany.hashMapKey());

            for (DBObject dbObject : dbObjects) {
                Object hashMapKey;

                if (Property.class.isAssignableFrom(hashMapKeyField.getType())) {
                    hashMapKey = ((Property) hashMapKeyField.get(dbObject)).getValue();
                } else {
                    hashMapKey = hashMapKeyField.get(dbObject);
                }

                referencedObjectMap.put(hashMapKey, dbObject);
            }
        } else {
            Collection referencedObjectList = (Collection) manyToManyField.get(parentObject);

            referencedObjectList.clear();
            referencedObjectList.addAll(dbObjects);
        }
    }

    private DBObject getObject(Class objClass, int id) throws Exception {
        DBObject dbObject = getDBObjectFromCache(objClass, id);

        if (dbObject == null) {
            String tableName = getTableName(objClass);
            Map<String, Object> objData = db.getObjectData(tableName, id);

            dbObject = createDBObject(objData, objClass, false);
        }

        return dbObject;
    }

    /**
     * Get referenced object in a many to many relationship
     *
     * @param throughTable
     * @param parentObjectClass
     * @param manyToMany
     * @param parentObjectID
     * @param recursive get referenced objects of the referenced object
     * @return
     * @throws Exception
     */
    public List<DBObject> getReferencedObjects(String throughTable, Class parentObjectClass, ManyToMany manyToMany, Integer parentObjectID, boolean recursive) throws Exception {
        String parentColumnName = getThroughTableColumnName(parentObjectClass, manyToMany.parentObjectColumnName());
        String referencedColumnName = getThroughTableColumnName(manyToMany.referencedObjectClass(), manyToMany.referencedObjectColumnName());
        String referencedObjectTableName = getTableName(manyToMany.referencedObjectClass());
        String whereClause = createManyToManyWhereClause(throughTable, parentColumnName, referencedColumnName, parentObjectID);
        List<Map<String, Object>> referencedObjectDataMaps = db.getObjects(referencedObjectTableName, whereClause);
        List<DBObject> dbObjects = new ArrayList<>();

        for (Map<String, Object> refObjectData : referencedObjectDataMaps) {
            dbObjects.add(createDBObject(refObjectData, manyToMany.referencedObjectClass(), !recursive));
        }

        return dbObjects;
    }

    private String createManyToManyWhereClause(String throughTable, String parentObjectColumnName, String referencedObjectColumnName, Integer parentTableID) {
        StringBuilder whereClause = new StringBuilder();

        whereClause.append(db.getPrimaryKeyColumnName()).append(" IN (")
                .append("SELECT ")
                .append(referencedObjectColumnName)
                .append(" FROM ")
                .append(throughTable)
                .append(" WHERE ")
                .append(parentObjectColumnName)
                .append(" = ")
                .append(parentTableID)
                .append(")");

        return whereClause.toString();
    }

    private <T extends DBObject> void handleForeignKeyField(ForeignKeyRequest request, T dbObject) throws Exception {
        Integer foreignKey = request.getForeignKeyID();
        Field field = request.getForeignKeyField();
        ForeignKey foreignKeyAnnotation = field.getAnnotation(ForeignKey.class);
        DBObject referencedObject = getObject(foreignKeyAnnotation.referencedObjectClass(), foreignKey);

        field.set(dbObject, referencedObject);
    }

    private class ForeignKeyRequest {

        private Field foreignKeyField;
        private Integer foreignKeyID;

        public ForeignKeyRequest(Field foreignKeyField, Integer foreignKeyID) {
            this.foreignKeyField = foreignKeyField;
            this.foreignKeyID = foreignKeyID;
        }

        public Field getForeignKeyField() {
            return foreignKeyField;
        }

        public Integer getForeignKeyID() {
            return foreignKeyID;
        }

    }
}
