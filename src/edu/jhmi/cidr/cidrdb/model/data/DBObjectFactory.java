/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.cidrdb.model.data;

import edu.jhmi.cidr.cidrdb.model.db.connections.Database;
import edu.jhmi.cidr.cidrdb.model.db.exceptions.DBException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * To do: Make object to table mapping nicer
 *
 * @author asanch15
 */
public class DBObjectFactory {
    
    public static final int APPLICATION = 1;
    public static final int CATEGORY = 2;
    private final Database db;
    private final Map<Class<? extends DBObject>, String> registeredClassesList;

//    private class ClassTableMapping {
//
//        public Class objClass;
//        public String tableName;
//
//        public ClassTableMapping(Class objClass, String tableName) {
//            this.objClass = objClass;
//            this.tableName = tableName;
//        }
//    }
    public DBObjectFactory(Database db) {
        this.db = db;
        this.registeredClassesList = new HashMap<>();

//        this.classObjIDList = new HashMap<>();
//
//        this.classObjIDList.put(Category.class, CATEGORY);
//        this.classObjIDList.put(Application.class, APPLICATION);
//
//        this.registeredClassesList.put(CATEGORY, new ClassTableMapping(Category.class, Category.class.getSimpleName()));
//        this.registeredClassesList.put(APPLICATION, new ClassTableMapping(Application.class, Application.class.getSimpleName()));
    }
    
    public void registerClass(Class<? extends DBObject> objClass) throws DBObjectException {
        if (!(DBObject.class.isAssignableFrom(objClass))) {
            throw new DBObjectException("Can not register a non-DBObject class");
        }
        
        registeredClassesList.put(objClass, objClass.getSimpleName().toLowerCase());
    }
    
    public boolean isRegistered(Class objClass) {
        if (registeredClassesList.containsKey(objClass)) {
            return true;
        }
        
        return false;
    }
    
    public DBObject createObject(Class objClass) throws DBObjectException {
        DBObject obj;
        
        if (!isRegistered(objClass)) {
            throw new DBObjectException("Unregistered DBObject subclass");
        }
        
        try {
            obj = (DBObject) objClass.getDeclaredConstructor().newInstance();
        } catch (Exception e) {
            throw new DBObjectException("Invalid DBObject: " + e.getMessage());
        }
        
        return obj;

//        switch (objID) {
//            case APPLICATION:
//                return new Application();
//            case CATEGORY:
//                return new Category();
//            default:
//                throw new DBObjectException("Invalid DBObject Type");
//        }
    }
    
    public DBObject getObject(Class objClass, int pk) throws DBObjectException {
        DBObject obj = createObject(objClass);
        
        try {
            Map<String, Object> objData =
                    db.getObjectData(registeredClassesList.get(objClass), pk);
            
            if (!objData.containsKey("pk")) {
                throw new DBObjectException("Object not found");
            }
            
            obj.mapDataToFields(objData);
        } catch (DBException e) {
            throw new DBObjectException("GetObject Failed: " + e.getMessage());
        }
        
        return obj;
    }
    
    public List<DBObject> getObjects(Class objClass) throws DBObjectException {
        List<DBObject> objList = new ArrayList<>();
        
        if (!isRegistered(objClass)) {
            throw new DBObjectException("Unregistered DBObject subclass");
        }
        
        try {
            List<Map<String, Object>> objDataMaps = db.getObjects(registeredClassesList.get(objClass));
            
            for (Map<String, Object> objData : objDataMaps) {
                DBObject obj = this.createObject(objClass);
                
                obj.mapDataToFields(objData);
                objList.add(obj);
            }
        } catch (DBException e) {
            throw new DBObjectException("GetObjects failed: " + e.getMessage());
        }
        
        return objList;
    }
    
    public void updateObject(DBObject obj) throws DBObjectException {
        Class objClass = obj.getClass();
        
        if (!isRegistered(objClass)) {
            throw new DBObjectException("Unregistered DBObject subclass");
        }
        
        try {
            db.updateObject(registeredClassesList.get(objClass), obj.getID(), obj.fieldsToMapData());
        } catch (DBException e) {
            throw new DBObjectException("UpdateObject failed: " + e.getMessage());
        }
    }
    
    public void deleteObject(DBObject obj) throws DBObjectException {
        Class objClass = obj.getClass();
        
        if (!isRegistered(objClass)) {
            throw new DBObjectException("Unregistered DBObject subclass");
        }
        
        try {
            db.deleteObject(registeredClassesList.get(objClass), obj.getID());
            obj = null;
        } catch (DBException e) {
            throw new DBObjectException("Delete Object failed: " + e.getMessage());
        }
    }
    
    public DBObject insertObject(DBObject obj) throws DBObjectException {
        Class objClass = obj.getClass();
        
        if (!isRegistered(objClass)) {
            throw new DBObjectException("Unregistered DBObject subclass");
        }
        
        try {
            int pk = db.insertObject(registeredClassesList.get(objClass), obj.fieldsToMapData());
            
            obj.setID(pk);
            return obj;
        } catch (DBException e) {
            throw new DBObjectException("InsertObject failed: " + e.getMessage());
        }
    }
    
    public DBObject cloneObject(DBObject obj) throws DBObjectException {
        DBObject cloneObject = createObject(obj.getClass());
        
        cloneObject.mapDataToFields(obj.fieldsToMapData());
        return cloneObject;
    }
}
