/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.cidrdb.model.data;

import edu.jhmi.cidr.cidrdb.Constants.DBConstants;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author asanch15
 */
public class DBObject {

    public int id;

    public DBObject() {
        this.id = DBConstants.NO_ID;
    }

    /**
     * @return the primary key
     */
    public int getID() {
        return id;
    }

    /**
     * Set object primary key
     *
     * @param id
     */
    protected void setID(int id) {
        this.id = id;
    }

    /**
     * Set object's fields to values in dataMap
     *
     * @param dataMap
     * @throws DBObjectException
     */
    protected void mapDataToFields(Map<String, Object> dataMap)
            throws DBObjectException {
        if (!dataMap.containsKey("pk")) {
            throw new DBObjectException("Invalid data map");
        }

        try {
            this.id = (int) dataMap.get("pk");
        } catch (ClassCastException e) {
            throw new DBObjectException("Invalid data map: " + e.getMessage());
        }

    }

    /**
     * Export object fields to a Map
     *
     * @return
     */
    protected Map<String, Object> fieldsToMapData() {
        Map<String, Object> mapData = new HashMap<>();

        mapData.put("id", this.id);

        return mapData;
    }

    @Override
    public String toString() {
        return this.fieldsToMapData().toString();
    }

    protected void clean() {
        this.id = DBConstants.NO_ID;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DBObject other = (DBObject) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    } 
}
