/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.jhmi.cidr.cidrdb.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author asanch15
 * @param <T>
 */
public class TransactionLog<T> {
    private final List<TransactionEvent<T>> events;

    public TransactionLog() {
        this.events = new ArrayList<>();
    }
    
    public void insert(T obj){
        events.add(new TransactionEvent<>(TransactionEventType.INSERT, obj));
    }
    
    public void delete(T obj){
        events.add(new TransactionEvent<>(TransactionEventType.DELETE, obj));
    }
    
    public void update(T obj){
        events.add(new TransactionEvent<>(TransactionEventType.UPDATE, obj));
    }
    
    public List<TransactionEvent<T>> getEvents(){
        return Collections.unmodifiableList(events);
    }
    
    public void mergeTransactionLog(TransactionLog<T> transactionLog){
        events.addAll(transactionLog.getEvents());
    }

    public enum TransactionEventType{
        INSERT, DELETE, UPDATE;
    }
    public class TransactionEvent<T>{
        private TransactionEventType eventType;
        private final T object;

        public TransactionEvent(TransactionEventType eventType, T object) {
            this.eventType = eventType;
            this.object = object;
        }

        /**
         * @return the eventType
         */
        public TransactionEventType getEventType() {
            return eventType;
        }

        /**
         * @return the object
         */
        public T getObject() {
            return object;
        }
    }
}
